package library.entities;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.IBook.BookState;
import library.entities.ILoan.LoanState;

class LibraryTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFine() {
		int expected = 1;
     	Book bookTest = new Book("","", " ",1);	
		Patron patronTest = new Patron("","","",3445,1);
		Date dueDate = new Date(System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000);
		Loan loan = new Loan(bookTest, patronTest, dueDate, LoanState.OVER_DUE, 1);
	    Library library= new Library(null,null,null,null,null,null,null,null);
		double actual = library.calculateOverDueFine(loan);
		assertEquals(expected,actual);
	}
	@Test
	void testCalculateOverDueFineLevied() {
        int expected = 2;
     	Book bookTest = new Book("","", "",3);	 
		Patron patronTest = new Patron("","","",1356,3);
		Date dueDate = new Date(System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000);
	    Loan loan = new Loan(bookTest, patronTest,dueDate , LoanState.OVER_DUE, 3);
		Library library= new Library(null,null,null,null,null,null,null,null);
		double actual = library.calculateOverDueFine(loan);
		assertEquals(expected,actual);
	}

}
